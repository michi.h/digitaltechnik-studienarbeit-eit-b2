# Digitaltechnik Studienarbeit

### Beschreibung der Aufgabe:
Entwerfen Sie eine Ampelsteuerung für eine Fußgängerampel (siehe Bild 1), die im Ruhezustand für
Autofahrer grün und für Fußgänger rot zeigt. Nach Drücken der Aufforderungen links oder rechts soll
dem Fußgänger das sichere Überqueren der Straße ermöglicht werden. Dazu wird ein „3-Sekunden-
Takt“ verwendet.

Legen Sie einen Automaten aus. Bestimmen Sie die Zustandsvariablen und Ansteuer- und
Ausgabegleichungen. Verifizieren und visualisieren Sie die Lösung mit einer Simulation in Vivado oder
z.B. mit Stateflow in MATLAB/Simulink.
Implementieren Sie eine Ampelsteuerung mit den Farben rot, grün und blau (für gelb) und der
vierten LED für das Rot der Fußgängerampel (das Grün der Fußgängerampel kann durch „Nicht Rot“
simuliert werden.) mit Hilfe des Spartan-Edge-Accelerator-Boards und des Breakout-Boards, das Sie
im Rahmen der E-Praxis bekommen haben.
**Abgabetermin: 14.09.2021**

### Abzugeben sind:
- Bericht (max. 20 Seiten), entweder im Learning Campus oder per E-Mail oder im Postkasten der Hochschule im Foyer (Im Falle der E-Mail bestätigt der Prüfer den Erhalt.)
- *.bit-datei oder kurzes Video, das die Steuerung funktioniert.
- Spartan-Edge-Accelerator-Boards und SD- Karte (d.h. Rückgabe an der Hochschule im September z.B. während des Praktikums Digitaltechnik)

### Bewertungskriterien für den Bericht

- Äußere Form (d.h. Gliederung, Literaturverzeichnis, Einleitung, Zusammenfassung, 2 Punkte)
- Theoretische Aufarbeitung des Automaten für die Ampelsteuerung (Grundlagen, 4 Punkte)
- Beschreibung des Versuchsaufbaus (d.h. Programmierung ESP32 zum Booten, Spartan-Edge-Accelerator-Boards, Vivado, etc. , 2 Punkte)
- Synthese, d.h. Beschreibung der konkreten Umsetzung der Aufgabe (2 Punkte)
- Verifikation, d.h. Simulation der Synthese, Programmierung einer „Test bench“, Dokumentation mit Hilfe eines Screenshots der Simulation (2 Punkte)
- Implementierung, Bit-file, Beschreibung des Ergebnisses (2 Punkte)
(Die Punkte beziehen sich jeweils auf Umfang (d.h. angemessener Umfang) und Inhalt (d.h. richtiger,
sinnvoller Inhalt)

**Summe: 14 Punkte**
